var searchData=
[
  ['convert_104',['Convert',['../class_wpf_app_1_1_u_i_1_1_berletidij_to_string_converter.html#a8cafa3d68266b7f148a4a8847e97222b',1,'WpfApp.UI.BerletidijToStringConverter.Convert()'],['../class_wpf_app_1_1_u_i_1_1_butorozott_to_brush_converter.html#a7ba5715f81232f097daf7d1b5ba98533',1,'WpfApp.UI.ButorozottToBrushConverter.Convert()']]],
  ['convertback_105',['ConvertBack',['../class_wpf_app_1_1_u_i_1_1_berletidij_to_string_converter.html#afedd53a5ed5687d836732b761ed6972b',1,'WpfApp.UI.BerletidijToStringConverter.ConvertBack()'],['../class_wpf_app_1_1_u_i_1_1_butorozott_to_brush_converter.html#a3ca95cd2fb6e8766ba25aa0fa50235bd',1,'WpfApp.UI.ButorozottToBrushConverter.ConvertBack()']]],
  ['copyfrom_106',['CopyFrom',['../class_wpf_app_1_1_data_1_1_ingatlan.html#a72cea16c48927fb7632e9c56f7f485e4',1,'WpfApp::Data::Ingatlan']]],
  ['create_107',['Create',['../interface_repo_1_1_i_repository.html#a77477cff2b3fecdb2f563b4f2959cebe',1,'Repo.IRepository.Create()'],['../class_repo_1_1_repository.html#a19ca3c8406f1ebe9c21dbc564b647d08',1,'Repo.Repository.Create()']]],
  ['createdelegate_108',['CreateDelegate',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#a8ec4c37e82d9f4e867e9655f4eac3a78',1,'XamlGeneratedNamespace::GeneratedInternalTypeHelper']]],
  ['createingatlan_109',['CreateIngatlan',['../class_logicc_1_1_business_logic.html#af1f279a5551979d08ec1df7e94f9c2e4',1,'Logicc.BusinessLogic.CreateIngatlan()'],['../interface_logicc_1_1_i_logic.html#a536d3b6ad6f23f840c8b63343f543786',1,'Logicc.ILogic.CreateIngatlan()']]],
  ['createinstance_110',['CreateInstance',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#aefb7a98fceb9c287cef4756942f441d1',1,'XamlGeneratedNamespace::GeneratedInternalTypeHelper']]]
];
