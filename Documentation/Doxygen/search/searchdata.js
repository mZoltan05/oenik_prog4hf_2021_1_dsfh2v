var indexSectionsWithContent =
{
  0: "abcdegiklmnoprstuwx",
  1: "abegimr",
  2: "dlrwx",
  3: "abcdegimorstu",
  4: "e",
  5: "gkp",
  6: "abdikmnt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "enums",
  5: "enumvalues",
  6: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Enumerations",
  5: "Enumerator",
  6: "Properties"
};

