var annotated_dup =
[
    [ "Data", "namespace_data.html", [
      [ "Models", "namespace_data_1_1_models.html", [
        [ "IngatlanContext", "class_data_1_1_models_1_1_ingatlan_context.html", "class_data_1_1_models_1_1_ingatlan_context" ],
        [ "Ingatlanok", "class_data_1_1_models_1_1_ingatlanok.html", "class_data_1_1_models_1_1_ingatlanok" ]
      ] ]
    ] ],
    [ "Logicc", "namespace_logicc.html", [
      [ "BusinessLogic", "class_logicc_1_1_business_logic.html", "class_logicc_1_1_business_logic" ],
      [ "ILogic", "interface_logicc_1_1_i_logic.html", "interface_logicc_1_1_i_logic" ]
    ] ],
    [ "Repo", "namespace_repo.html", [
      [ "IRepository", "interface_repo_1_1_i_repository.html", "interface_repo_1_1_i_repository" ],
      [ "Repository", "class_repo_1_1_repository.html", "class_repo_1_1_repository" ]
    ] ],
    [ "WpfApp", "namespace_wpf_app.html", [
      [ "BL", "namespace_wpf_app_1_1_b_l.html", [
        [ "IEditorService", "interface_wpf_app_1_1_b_l_1_1_i_editor_service.html", "interface_wpf_app_1_1_b_l_1_1_i_editor_service" ],
        [ "IIngatlanLogic", "interface_wpf_app_1_1_b_l_1_1_i_ingatlan_logic.html", "interface_wpf_app_1_1_b_l_1_1_i_ingatlan_logic" ],
        [ "IngatlanLogic", "class_wpf_app_1_1_b_l_1_1_ingatlan_logic.html", "class_wpf_app_1_1_b_l_1_1_ingatlan_logic" ]
      ] ],
      [ "Data", "namespace_wpf_app_1_1_data.html", [
        [ "Ingatlan", "class_wpf_app_1_1_data_1_1_ingatlan.html", "class_wpf_app_1_1_data_1_1_ingatlan" ]
      ] ],
      [ "UI", "namespace_wpf_app_1_1_u_i.html", [
        [ "EditorWindow", "class_wpf_app_1_1_u_i_1_1_editor_window.html", "class_wpf_app_1_1_u_i_1_1_editor_window" ],
        [ "BerletidijToStringConverter", "class_wpf_app_1_1_u_i_1_1_berletidij_to_string_converter.html", "class_wpf_app_1_1_u_i_1_1_berletidij_to_string_converter" ],
        [ "ButorozottToBrushConverter", "class_wpf_app_1_1_u_i_1_1_butorozott_to_brush_converter.html", "class_wpf_app_1_1_u_i_1_1_butorozott_to_brush_converter" ],
        [ "EditorServiceViaWindow", "class_wpf_app_1_1_u_i_1_1_editor_service_via_window.html", "class_wpf_app_1_1_u_i_1_1_editor_service_via_window" ]
      ] ],
      [ "VM", "namespace_wpf_app_1_1_v_m.html", [
        [ "EditorViewModel", "class_wpf_app_1_1_v_m_1_1_editor_view_model.html", "class_wpf_app_1_1_v_m_1_1_editor_view_model" ],
        [ "MainViewModel", "class_wpf_app_1_1_v_m_1_1_main_view_model.html", "class_wpf_app_1_1_v_m_1_1_main_view_model" ]
      ] ],
      [ "App", "class_wpf_app_1_1_app.html", "class_wpf_app_1_1_app" ],
      [ "MainWindow", "class_wpf_app_1_1_main_window.html", "class_wpf_app_1_1_main_window" ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];