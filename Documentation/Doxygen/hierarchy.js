var hierarchy =
[
    [ "Application", null, [
      [ "WpfApp.App", "class_wpf_app_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "WpfApp.App", "class_wpf_app_1_1_app.html", null ],
      [ "WpfApp.App", "class_wpf_app_1_1_app.html", null ],
      [ "WpfApp.App", "class_wpf_app_1_1_app.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "Data.Models.IngatlanContext", "class_data_1_1_models_1_1_ingatlan_context.html", null ]
    ] ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "WpfApp.MainWindow", "class_wpf_app_1_1_main_window.html", null ],
      [ "WpfApp.MainWindow", "class_wpf_app_1_1_main_window.html", null ],
      [ "WpfApp.MainWindow", "class_wpf_app_1_1_main_window.html", null ],
      [ "WpfApp.UI.EditorWindow", "class_wpf_app_1_1_u_i_1_1_editor_window.html", null ],
      [ "WpfApp.UI.EditorWindow", "class_wpf_app_1_1_u_i_1_1_editor_window.html", null ]
    ] ],
    [ "WpfApp.BL.IEditorService", "interface_wpf_app_1_1_b_l_1_1_i_editor_service.html", [
      [ "WpfApp.UI.EditorServiceViaWindow", "class_wpf_app_1_1_u_i_1_1_editor_service_via_window.html", null ]
    ] ],
    [ "WpfApp.BL.IIngatlanLogic", "interface_wpf_app_1_1_b_l_1_1_i_ingatlan_logic.html", [
      [ "WpfApp.BL.IngatlanLogic", "class_wpf_app_1_1_b_l_1_1_ingatlan_logic.html", null ]
    ] ],
    [ "Logicc.ILogic", "interface_logicc_1_1_i_logic.html", [
      [ "Logicc.BusinessLogic", "class_logicc_1_1_business_logic.html", null ]
    ] ],
    [ "Data.Models.Ingatlanok", "class_data_1_1_models_1_1_ingatlanok.html", null ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Repo.IRepository< T >", "interface_repo_1_1_i_repository.html", [
      [ "Repo.Repository< T >", "class_repo_1_1_repository.html", null ]
    ] ],
    [ "Repo.IRepository< Data.Models.Ingatlanok >", "interface_repo_1_1_i_repository.html", null ],
    [ "IValueConverter", null, [
      [ "WpfApp.UI.BerletidijToStringConverter", "class_wpf_app_1_1_u_i_1_1_berletidij_to_string_converter.html", null ],
      [ "WpfApp.UI.ButorozottToBrushConverter", "class_wpf_app_1_1_u_i_1_1_butorozott_to_brush_converter.html", null ]
    ] ],
    [ "ObservableObject", null, [
      [ "WpfApp.Data.Ingatlan", "class_wpf_app_1_1_data_1_1_ingatlan.html", null ]
    ] ],
    [ "ViewModelBase", null, [
      [ "WpfApp.VM.EditorViewModel", "class_wpf_app_1_1_v_m_1_1_editor_view_model.html", null ],
      [ "WpfApp.VM.MainViewModel", "class_wpf_app_1_1_v_m_1_1_main_view_model.html", null ]
    ] ],
    [ "System.Windows.Window", null, [
      [ "WpfApp.MainWindow", "class_wpf_app_1_1_main_window.html", null ],
      [ "WpfApp.MainWindow", "class_wpf_app_1_1_main_window.html", null ],
      [ "WpfApp.MainWindow", "class_wpf_app_1_1_main_window.html", null ],
      [ "WpfApp.UI.EditorWindow", "class_wpf_app_1_1_u_i_1_1_editor_window.html", null ],
      [ "WpfApp.UI.EditorWindow", "class_wpf_app_1_1_u_i_1_1_editor_window.html", null ],
      [ "WpfApp.UI.EditorWindow", "class_wpf_app_1_1_u_i_1_1_editor_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "WpfApp.MainWindow", "class_wpf_app_1_1_main_window.html", null ]
    ] ]
];