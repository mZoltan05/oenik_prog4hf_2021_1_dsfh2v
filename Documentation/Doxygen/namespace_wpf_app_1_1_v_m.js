var namespace_wpf_app_1_1_v_m =
[
    [ "EditorViewModel", "class_wpf_app_1_1_v_m_1_1_editor_view_model.html", "class_wpf_app_1_1_v_m_1_1_editor_view_model" ],
    [ "MainViewModel", "class_wpf_app_1_1_v_m_1_1_main_view_model.html", "class_wpf_app_1_1_v_m_1_1_main_view_model" ],
    [ "EnumIngatlanType", "namespace_wpf_app_1_1_v_m.html#ac6cfc91fc4d096872c6dc579f4df09da", [
      [ "Panel", "namespace_wpf_app_1_1_v_m.html#ac6cfc91fc4d096872c6dc579f4df09daab8da6df14bf06283cbf588df6998722e", null ],
      [ "Kertesház", "namespace_wpf_app_1_1_v_m.html#ac6cfc91fc4d096872c6dc579f4df09daa5ca145586729b8b3c182e65db3b83ac2", null ],
      [ "Garázs", "namespace_wpf_app_1_1_v_m.html#ac6cfc91fc4d096872c6dc579f4df09daa31fc20afd98f0ef19153af3240f08ce9", null ]
    ] ]
];