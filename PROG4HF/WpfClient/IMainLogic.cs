﻿// <copyright file="IMainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Main logic.
    /// </summary>
    public interface IMainLogic
    {
        /// <summary>
        /// Sender.
        /// </summary>
        /// <param name="success">is it success.</param>
        public void SendMessage(bool success);

        /// <summary>
        /// Get all ingatlan.
        /// </summary>
        /// <returns>list of ingatlans.</returns>
        public IList<IngatlanVM> ApiGetIngatlans();

        /// <summary>
        /// Del ingatlan.
        /// </summary>
        /// <param name="ingatlan">ingatlan to delete.</param>
        public void ApiDelIngatlan(IngatlanVM ingatlan);

        /// <summary>
        /// Edit an ingatlan.
        /// </summary>
        /// <param name="ingatlan">ingatlan to edit.</param>
        /// <param name="isEditing">is editing.</param>
        /// <returns>success.</returns>
        public bool ApiEditIngatlan(IngatlanVM ingatlan, bool isEditing);

        /// <summary>
        /// Edit an ingatlan.
        /// </summary>
        /// <param name="ingatlan">ingatlan to edit.</param>
        /// <param name="editorFunc">how to edit.</param>
        public void EditIngatlan(IngatlanVM ingatlan, Func<IngatlanVM, bool> editorFunc);
    }
}
