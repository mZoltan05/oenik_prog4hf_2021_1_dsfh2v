﻿// <copyright file="IngatlanVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data.Models;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Ingatlan ViewModel.
    /// </summary>
    public class IngatlanVM : ObservableObject
    {
        private decimal ingatlanid;
        private string tipus;
        private string menedzser;
        private decimal kerulet;
        private bool butorozott;
        private decimal berletidij;
        private decimal negyzetmeter;

        /// <summary>
        /// Gets or sets IngatlanId proprerty of Ingatlan entity.
        /// </summary>
        public decimal IngatlanId
        {
            get { return this.ingatlanid; }
            set { this.Set(ref this.ingatlanid, value); }
        }

        /// <summary>
        /// Gets or sets Tipus proprerty of Ingatlan entity.
        /// </summary>
        public string Tipus
        {
            get { return this.tipus; }
            set { this.Set(ref this.tipus, value); }
        }

        /// <summary>
        /// Gets or sets Menedzser proprerty of Ingatlan entity.
        /// </summary>
        public string Menedzser
        {
            get { return this.menedzser; }
            set { this.Set(ref this.menedzser, value); }
        }

        /// <summary>
        /// Gets or sets Kerulet proprerty of Ingatlan entity.
        /// </summary>
        public decimal Kerulet
        {
            get { return this.kerulet; }
            set { this.Set(ref this.kerulet, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether.
        /// </summary>
        public bool Butorozott
        {
            get { return this.butorozott; }
            set { this.Set(ref this.butorozott, value); }
        }

        /// <summary>
        /// Gets or sets BerletiDij proprerty of Ingatlan entity.
        /// </summary>
        public decimal BerletiDij
        {
            get { return this.berletidij; }
            set { this.Set(ref this.berletidij, value); }
        }

        /// <summary>
        /// Gets or sets Negyzetmeter proprerty of Ingatlan entity.
        /// </summary>
        public decimal Negyzetmeter
        {
            get { return this.negyzetmeter; }
            set { this.Set(ref this.negyzetmeter, value); }
        }

        /// <summary>
        /// Convert VM entity to a DB entity.
        /// </summary>
        /// <returns>A DB entity.</returns>
        public Ingatlanok ConvertToDBEntity()
        {
            Ingatlanok vissza = new Ingatlanok();
            vissza.BerletiDij = this.BerletiDij;
            if (this.Butorozott)
            {
                vissza.Butorozott = 0;
            }
            else
            {
                vissza.Butorozott = 1;
            }

            vissza.IngatlanId = this.IngatlanId;
            vissza.Kerulet = this.Kerulet;
            vissza.Menedzser = this.Menedzser;
            vissza.Negyzetmeter = this.Negyzetmeter;
            switch (this.Tipus)
            {
                case "Panel":
                    vissza.Tipus = "Panel";
                    break;
                case "Kertesház":
                    vissza.Tipus = "Kertesház";
                    break;
                case "Garázs":
                    vissza.Tipus = "Garázs";
                    break;
            }

            return vissza;
        }

        /// <summary>
        /// Makes a copy of this entity.
        /// </summary>
        /// <param name="other">Object to copy.</param>
        public void CopyFrom(IngatlanVM other)
        {
            if (other == null)
            {
                return;
            }

            this.Negyzetmeter = other.Negyzetmeter;
            this.Menedzser = other.Menedzser;
            this.Kerulet = other.Kerulet;
            this.BerletiDij = other.BerletiDij;
            this.Butorozott = other.Butorozott;
            this.Tipus = other.Tipus;
            this.IngatlanId = other.IngatlanId;
        }
    }
}
