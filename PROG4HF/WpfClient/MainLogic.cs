﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Logic class.
    /// </summary>
    public class MainLogic : IMainLogic
    {
        private string url = "http://localhost:49219/ingatlansapi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// Sending a message.
        /// </summary>
        /// <param name="success">Is it success.</param>
        public void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "IngatlanResult");
        }

        /// <summary>
        /// Get ingatlans through the api.
        /// </summary>
        /// <returns>Ingatlan list.</returns>
        public IList<IngatlanVM> ApiGetIngatlans()
        {
            string json = this.client.GetStringAsync(new Uri(this.url + "all")).Result;
            var list = JsonSerializer.Deserialize<List<IngatlanVM>>(json, this.jsonOptions);
            return list;
        }

        /// <summary>
        /// Delete an ingatlan.
        /// </summary>
        /// <param name="ingatlan">IngatlanVM.</param>
        public void ApiDelIngatlan(IngatlanVM ingatlan)
        {
            bool success = false;
            if (ingatlan != null)
            {
                string json = this.client.GetStringAsync(new Uri(this.url + "del/" + ingatlan.IngatlanId.ToString(CultureInfo.CurrentCulture))).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            this.SendMessage(success);
        }

        /// <summary>
        /// Edit an instance of ingatlan.
        /// </summary>
        /// <param name="ingatlan">IngatlanVM.</param>
        /// <param name="isEditing">Is Editing.</param>
        /// <returns>success.</returns>
        public bool ApiEditIngatlan(IngatlanVM ingatlan, bool isEditing)
        {
            if (ingatlan == null)
            {
                return false;
            }

            string myUrl = this.url + (isEditing ? "mod" : "add");
            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("id", ingatlan.IngatlanId.ToString(CultureInfo.CurrentCulture));
            }

            postData.Add("negyzetmeter", ingatlan.Negyzetmeter.ToString(CultureInfo.CurrentCulture));
            postData.Add("tipus", ingatlan.Tipus.ToString(CultureInfo.CurrentCulture));
            postData.Add("menedzser", ingatlan.Menedzser.ToString(CultureInfo.CurrentCulture));
            postData.Add("kerulet", ingatlan.Kerulet.ToString(CultureInfo.CurrentCulture));
            postData.Add("butorozott", ingatlan.Butorozott.ToString(CultureInfo.CurrentCulture));
            postData.Add("berletidij", ingatlan.BerletiDij.ToString(CultureInfo.CurrentCulture));
            string json = this.client.PostAsync(new Uri(myUrl), new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }

        /// <summary>
        /// Edit and ingatlan.
        /// </summary>
        /// <param name="ingatlan">Ingatlan to edit.</param>
        /// <param name="editorFunc">Function.</param>>
        public void EditIngatlan(IngatlanVM ingatlan, Func<IngatlanVM, bool> editorFunc)
        {
            IngatlanVM clone = new IngatlanVM();
            if (ingatlan != null)
            {
                clone.CopyFrom(ingatlan);
            }

            bool? success = editorFunc?.Invoke(clone);
            if (success == true)
            {
                if (ingatlan != null)
                {
                    success = this.ApiEditIngatlan(clone, true);
                }
                else
                {
                    success = this.ApiEditIngatlan(clone, false);
                }

                this.SendMessage(success == true);
            }
        }
    }
}
