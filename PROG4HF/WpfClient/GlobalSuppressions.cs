﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: System.CLSCompliant(false)]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "<NikGitStats>", Scope = "type", Target = "~T:WpfClient.MainLogic")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<NikGitStats>", Scope = "member", Target = "~M:WpfClient.MainLogic.ApiEditIngatlan(WpfClient.IngatlanVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<NikGitStats>", Scope = "member", Target = "~P:WpfClient.MainVM.AllIngatlan")]
