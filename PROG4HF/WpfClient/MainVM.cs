﻿// <copyright file="MainVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// Main viewModel.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private IngatlanVM selectedIngatlan;
        private ObservableCollection<IngatlanVM> allIngatlan;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        /// <param name="logic">main logic.</param>
        public MainVM(IMainLogic logic)
        {
            this.logic = (MainLogic)logic;
            this.LoadCmd = new RelayCommand(() => this.AllIngatlan = new ObservableCollection<IngatlanVM>(logic.ApiGetIngatlans()));
            this.DelCmd = new RelayCommand(() => logic.ApiDelIngatlan(this.SelectedIngatlan));
            this.AddCmd = new RelayCommand(() => logic.EditIngatlan(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() => logic.EditIngatlan(this.selectedIngatlan, this.EditorFunc));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IMainLogic>())
        {
        }

        /// <summary>
        /// Gets or sets selected ingatlan.
        /// </summary>
        public IngatlanVM SelectedIngatlan
        {
            get { return this.selectedIngatlan; }
            set { this.Set(ref this.selectedIngatlan, value); }
        }

        /// <summary>
        /// Gets or sets all ingatlan.
        /// </summary>
        public ObservableCollection<IngatlanVM> AllIngatlan
        {
            get { return this.allIngatlan; }
            set { this.Set(ref this.allIngatlan, value); }
        }

        /// <summary>
        /// Gets or sets how to edit an ingatlan.
        /// </summary>
        public Func<IngatlanVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets add command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets del command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets mod command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets load command.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
