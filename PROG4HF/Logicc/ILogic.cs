﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logicc
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data.Models;

    /// <summary>
    /// This interface does the logical tasks before data send forward to the Repository.
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// This method sends the recently created entity to the create method.
        /// </summary>
        /// <param name="uj">The recently created ingatlan entity.</param>
        void CreateIngatlan(Ingatlanok uj);

        /// <summary>
        /// This method reads all Ingatlan entities.
        /// </summary>
        /// <returns>The ingatlan collection.</returns>
        IQueryable<Ingatlanok> ReadIngatlan();

        /// <summary>
        /// This method update an entity.
        /// </summary>
        /// <param name="id">Id of the ingatlan to update.</param>
        /// <param name="attributeName">Name of the attribute to change.</param>
        /// <param name="newValue">The new value of the attribute.</param>
        void UpdateIngatlan(string id, string attributeName, string newValue);

        /// <summary>
        /// This method removes an ingatlan entity by id.
        /// </summary>
        /// <param name="id">Id of the ingatlan to remove.</param>
        void DeleteIngatlan(string id);

        /// <summary>
        /// Get one ingatlan.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>ingatlan entity.</returns>
        Ingatlanok GetOneIngatlan(int id);
    }
}