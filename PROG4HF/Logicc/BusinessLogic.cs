﻿// <copyright file="BusinessLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logicc
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using Data.Models;
    using Repo;

    /// <summary>
    /// Business Logic.
    /// </summary>
    public class BusinessLogic : ILogic
    {
        private readonly IRepository<Ingatlanok> ingatlanok;

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessLogic"/> class.
        /// </summary>
        public BusinessLogic()
        {
            this.ingatlanok = new Repository<Ingatlanok>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessLogic"/> class.
        /// </summary>
        /// <param name="ingatlanr">Ingatlan repository.</param>
        public BusinessLogic(IRepository<Ingatlanok> ingatlanr)
        {
            this.ingatlanok = ingatlanr;
        }

        /// <inheritdoc/>
        public void CreateIngatlan(Ingatlanok uj)
        {
            this.ingatlanok.Create(uj);
        }

        /// <inheritdoc/>
        public void DeleteIngatlan(string id)
        {
            this.ingatlanok.Delete(id);
        }

        /// <inheritdoc/>
        public IQueryable<Ingatlanok> ReadIngatlan()
        {
            return this.ingatlanok.Read();
        }

        /// <summary>
        /// Returns with an ingatlan.
        /// </summary>
        /// <param name="id">ID.</param>
        /// <returns>Ingatlan.</returns>
        public Ingatlanok GetOneIngatlan(int id)
        {
            var x = this.ReadIngatlan();
            foreach (Ingatlanok item in x)
            {
                if (item.IngatlanId == Convert.ToDecimal(id, CultureInfo.CurrentCulture))
                {
                    return item;
                }
            }

            return this.ReadIngatlan().First();
        }

        /// <inheritdoc/>
        public void UpdateIngatlan(string id, string attributeName, string newValue)
        {
            this.ingatlanok.Update(id, attributeName, newValue);
        }
    }
}
