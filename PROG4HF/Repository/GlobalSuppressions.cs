﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<NikGitStats>", Scope = "member", Target = "~M:Repository.Repository`1.Update(System.String,System.String,System.String)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<NikGitStats>", Scope = "member", Target = "~M:Repository.Repository`1.Delete(System.String)")]
[assembly: System.CLSCompliant(false)]