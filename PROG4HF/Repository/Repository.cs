﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using Data.Models;

    /// <summary>
    /// Repository.
    /// </summary>
    /// <typeparam name="T">T type.</typeparam>
    public class Repository<T> : IRepository<T>
        where T : class
    {
        private static readonly IngatlanContext DataBase = new IngatlanContext();

        /// <inheritdoc/>
        public void Create(T tartalom)
        {
            DataBase.Set<T>().Add(tartalom);
            DataBase.SaveChanges();
        }

        /// <inheritdoc/>
        public void Delete(string id)
        {
            CultureInfo provider = new CultureInfo("hu-HU");
            string idName = typeof(T).GetProperties().Select(x => x.Name).ToList().First(y => y.Contains("Id", StringComparison.Ordinal));
            PropertyInfo typeId = typeof(T).GetProperty(idName);
            var parameter = Expression.Parameter(typeof(T), "x");
            var left = Expression.PropertyOrField(parameter, idName);
            var right = Expression.Constant(Convert.ChangeType(id, typeId.PropertyType, provider), left.Type);
            var condition = Expression.Equal(left, right);
            var predicate = Expression.Lambda<Func<T, bool>>(condition, parameter);
            var result = DataBase.Set<T>().Single(predicate);
            DataBase.Set<T>().Remove(result);
            DataBase.SaveChanges();
        }

        /// <inheritdoc/>
        public IQueryable<T> Read()
        {
            return DataBase.Set<T>().Select(x => x);
        }

        /// <inheritdoc/>
        public void Update(string id, string attributeName, string newValue)
        {
            CultureInfo provider = new CultureInfo("hu-HU");
            string idName = typeof(T).GetProperties().Select(x => x.Name).ToList().First(y => y.Contains("Id", StringComparison.Ordinal));
            PropertyInfo typeId = typeof(T).GetProperty(idName);
            PropertyInfo typeAttribute = typeof(T).GetProperty(attributeName);
            var parameter = Expression.Parameter(typeof(T), "x");
            var left = Expression.PropertyOrField(parameter, idName);
            var right = Expression.Constant(Convert.ChangeType(id, typeId.PropertyType, provider), left.Type);
            var condition = Expression.Equal(left, right);
            var predicate = Expression.Lambda<Func<T, bool>>(condition, parameter);
            var item = DataBase.Set<T>().Single(predicate);
            object finalValue = Convert.ChangeType(newValue, typeAttribute.PropertyType, provider);
            item.GetType().GetProperty(attributeName).SetValue(item, finalValue);
            DataBase.SaveChanges();
        }
    }
}
