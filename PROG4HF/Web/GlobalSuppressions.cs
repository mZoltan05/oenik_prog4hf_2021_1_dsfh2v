﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: System.CLSCompliant(false)]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<NikGitStats>", Scope = "member", Target = "~P:Web.Models.CarListViewModel.GetIngatlans")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<NikGitStats>", Scope = "member", Target = "~P:Web.Models.IngatlanListViewModel.GetIngatlans")]
