﻿// <copyright file="IngatlansAPIController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Logicc;
    using Microsoft.AspNetCore.Mvc;
    using Web.Models;

    /// <summary>
    /// API controller.
    /// </summary>
    public class IngatlansAPIController : Controller
    {
        private ILogic logic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="IngatlansAPIController"/> class.
        /// </summary>
        /// <param name="logic">Ingatlan Logic.</param>
        /// <param name="mapper">Mapper.</param>
        public IngatlansAPIController(ILogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Get all Ingatlan.
        /// </summary>
        /// <returns>List of ingatlanok.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Ingatlan> GetAll()
        {
            var ingatlanok = this.logic.ReadIngatlan();
            List<Models.Ingatlan> re = new List<Models.Ingatlan>();
            foreach (Data.Models.Ingatlanok item in ingatlanok)
            {
                re.Add(this.mapper.Map<Data.Models.Ingatlanok, Models.Ingatlan>(item));
            }

            return re;
        }

        // get ingatlansapi/del/5

        /// <summary>
        /// Dete one ingatlan.
        /// </summary>
        /// <param name="id">Id of ingatlan to delete.</param>
        /// <returns>Result of operation.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DelOneIngatlan(int id)
        {
            this.logic.DeleteIngatlan(Convert.ToString(id, CultureInfo.CurrentCulture));
            return new ApiResult()
            {
                OperationResult = true,
            };
        }

        // post ingatlansapi/add + post 1 ingatlan

        /// <summary>
        /// Create a new ingatlan.
        /// </summary>
        /// <param name="ingatlan">Type of ingatlan.</param>
        /// <returns>Operation result.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneIngatlan(Models.Ingatlan ingatlan)
        {
            bool success = true;
            try
            {
                this.logic.CreateIngatlan(this.mapper.Map<Models.Ingatlan, Data.Models.Ingatlanok>(ingatlan));
            }
            catch (ArgumentException)
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }

        // post ingatlansapi/mod + post 1 ingatlan

        /// <summary>
        /// Modify an ingatlan.
        /// </summary>
        /// <param name="ingatlan">Type of ingatlan.</param>
        /// <returns>Operation result.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneIngatlan(Models.Ingatlan ingatlan)
        {
            if (ingatlan != null)
            {
                this.logic.UpdateIngatlan(ingatlan.Id.ToString(CultureInfo.CurrentCulture), "Menedzser", ingatlan.Manager);
                this.logic.UpdateIngatlan(ingatlan.Id.ToString(CultureInfo.CurrentCulture), "BerletiDij", ingatlan.RentFee.ToString(CultureInfo.CurrentCulture));
                return new ApiResult() { OperationResult = true };
            }

            return new ApiResult() { OperationResult = false };
        }
    }
}
