﻿// <copyright file="IngatlansController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Logicc;
    using Microsoft.AspNetCore.Mvc;
    using Web.Models;

    /// <summary>
    /// Controller.
    /// </summary>
    public class IngatlansController : Controller
    {
        private ILogic logic;
        private IMapper mapper;
        private IngatlanListViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="IngatlansController"/> class.
        /// </summary>
        /// <param name="logic">Ilogic.</param>
        /// <param name="mapper">IMapper.</param>
        public IngatlansController(ILogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;

            this.vm = new IngatlanListViewModel();
            this.vm.EditedIngatlan = new Models.Ingatlan();
            var ingatlanok = this.logic.ReadIngatlan();
            this.vm.GetIngatlans = new Collection<Ingatlan>();
            foreach (var item in ingatlanok)
            {
                if (mapper is not null)
                {
                    this.vm.GetIngatlans.Add(mapper.Map<Data.Models.Ingatlanok, Ingatlan>(item));
                }
            }
        }

        /// <summary>
        /// Index action.
        /// </summary>
        /// <returns>view.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("IngatlanIndex", this.vm);
        }

        /// <summary>
        /// Details action.
        /// </summary>
        /// <returns>view.</returns>
        /// <param name="id">ID.</param>
        public IActionResult Details(int id)
        {
            return this.View("IngatlansDetails", this.GetIngatlanModel(id));
        }

        /// <summary>
        /// Remove action.
        /// </summary>
        /// <returns>view.</returns>
        /// <param name="id">ID.</param>
        public IActionResult Remove(int id)
        {
            string i = id.ToString(CultureInfo.CurrentCulture);
            this.logic.DeleteIngatlan(i);
            this.TempData["editResult"] = "Delete OK";
            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Edit action.
        /// </summary>
        /// <returns>view.</returns>
        /// <param name="id">ID.</param>
        public IActionResult Edit(int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.vm.EditedIngatlan = this.GetIngatlanModel(id);
            return this.View("IngatlanIndex", this.vm);
        }

        /// <summary>
        /// Edit.
        /// </summary>
        /// <param name="ingatlan">Ingatlan Entity.</param>
        /// <param name="editAction">Action.</param>
        /// <returns>View.</returns>
        [HttpPost]
        public IActionResult Edit(Models.Ingatlan ingatlan, string editAction)
        {
            if (this.ModelState.IsValid && ingatlan != null)
            {
                this.TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    Data.Models.Ingatlanok ing = new Data.Models.Ingatlanok();
                    ing.BerletiDij = ingatlan.RentFee;
                    ing.Butorozott = ingatlan.IsFurnished;
                    ing.Kerulet = ingatlan.District;
                    ing.Menedzser = ingatlan.Manager;
                    ing.Negyzetmeter = ingatlan.SqrMeter;
                    ing.Tipus = ingatlan.Type;
                    this.logic.CreateIngatlan(ing);
                }
                else
                {
                    this.logic.UpdateIngatlan(ingatlan.Id.ToString(CultureInfo.CurrentCulture), "Menedzser", ingatlan.Manager);
                    this.logic.UpdateIngatlan(ingatlan.Id.ToString(CultureInfo.CurrentCulture), "BerletiDij", ingatlan.RentFee.ToString(CultureInfo.CurrentCulture));
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.vm.EditedIngatlan = ingatlan;
                return this.View("IngatlanIndex", this.vm);
            }
        }

        private Models.Ingatlan GetIngatlanModel(int id)
        {
            Data.Models.Ingatlanok ing = this.logic.GetOneIngatlan(id);
            return this.mapper.Map<Data.Models.Ingatlanok, Models.Ingatlan>(ing);
        }
    }
}
