﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Data.Models;

    /// <summary>
    /// Map between models.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// Mapper method.
        /// </summary>
        /// <returns>A mapper.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Ingatlanok, Ingatlan>().
                ForMember(dest => dest.Id, map => map.MapFrom(src => src.IngatlanId)).
                ForMember(dest => dest.District, map => map.MapFrom(src => src.Kerulet)).
                ForMember(dest => dest.IsFurnished, map => map.MapFrom(src => src.Butorozott)).
                ForMember(dest => dest.Manager, map => map.MapFrom(src => src.Menedzser)).
                ForMember(dest => dest.RentFee, map => map.MapFrom(src => src.BerletiDij)).
                ForMember(dest => dest.SqrMeter, map => map.MapFrom(src => src.Negyzetmeter)).
                ForMember(dest => dest.Type, map => map.MapFrom(src => src.Tipus)).ReverseMap();
            });
            return config.CreateMapper();
        }
    }
}
