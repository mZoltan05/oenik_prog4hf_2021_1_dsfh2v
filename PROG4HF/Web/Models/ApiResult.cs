﻿// <copyright file="ApiResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Logicc;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Result of api.
    /// </summary>
    public class ApiResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether the result of the operation.
        /// </summary>
        public bool OperationResult { get; set; }
    }
}
