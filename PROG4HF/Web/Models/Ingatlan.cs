﻿// <copyright file="Ingatlan.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Ingatlan Type: Form Model / MVC Model.
    /// </summary>
    public class Ingatlan
    {
        /// <summary>
        /// Gets or sets id of the ingatlan.
        /// </summary>
        [Display(Name = "Ingatlan Id")]
        public decimal Id { get; set; }

        /// <summary>
        /// Gets or sets type of ingatlan.
        /// </summary>
        [Display(Name = "Ingatlan Típus")]
        [Required]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets manager of ingatlan.
        /// </summary>
        [Display(Name = "Menedzser")]
        [Required]
        public string Manager { get; set; }

        /// <summary>
        /// Gets or sets district of ingatlan's location.
        /// </summary>
        [Display(Name = "Kerület")]
        [Required]
        public decimal District { get; set; }

        /// <summary>
        /// Gets or sets value is furnished.
        /// </summary>
        [Display(Name = "Bútorozott")]
        public decimal IsFurnished { get; set; }

        /// <summary>
        /// Gets or sets renting fee of ingatlan.
        /// </summary>
        [Display(Name = "Bérleti Díj")]
        [Required]
        public decimal RentFee { get; set; }

        /// <summary>
        /// Gets or sets square meter of ingatlan.
        /// </summary>
        [Display(Name = "Négyzetméter")]
        [Required]
        public decimal SqrMeter { get; set; }
    }
}
