﻿// <copyright file="IngatlanListViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// This is how the Ingatlan List will be displayed.
    /// </summary>
    public class IngatlanListViewModel
    {
        /// <summary>
        /// Gets or sets Ingatlan entity to display.
        /// </summary>
        public Ingatlan EditedIngatlan { get; set; }

        /// <summary>
        /// Gets or sets List of ingatlan.
        /// </summary>
        public Collection<Ingatlan> GetIngatlans { get; set; }
    }
}
