﻿// <copyright file="Ingatlan.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp.Data
{
    using System;
    using GalaSoft.MvvmLight;
    using global::Data.Models;
    using WpfApp.VM;

    /// <summary>
    /// Ingatlan class.
    /// </summary>
    public class Ingatlan : ObservableObject
    {
        private decimal ingatlanid;
        private EnumIngatlanType tipus;
        private string menedzser;
        private decimal kerulet;
        private bool butorozott;
        private decimal berletidij;
        private decimal negyzetmeter;

        /// <summary>
        /// Gets or sets IngatlanId proprerty of Ingatlan entity.
        /// </summary>
        public decimal IngatlanId
        {
            get { return this.ingatlanid; } set { this.Set(ref this.ingatlanid, value); }
        }

        /// <summary>
        /// Gets or sets Tipus proprerty of Ingatlan entity.
        /// </summary>
        public EnumIngatlanType Tipus
        {
            get { return this.tipus; }
            set { this.Set(ref this.tipus, value); }
        }

        /// <summary>
        /// Gets or sets Menedzser proprerty of Ingatlan entity.
        /// </summary>
        public string Menedzser
        {
            get { return this.menedzser; }
            set { this.Set(ref this.menedzser, value); }
        }

        /// <summary>
        /// Gets or sets Kerulet proprerty of Ingatlan entity.
        /// </summary>
        public decimal Kerulet
        {
            get { return this.kerulet; }
            set { this.Set(ref this.kerulet, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether.
        /// </summary>
        public bool Butorozott
        {
            get { return this.butorozott; }
            set { this.Set(ref this.butorozott, value); }
        }

        /// <summary>
        /// Gets or sets BerletiDij proprerty of Ingatlan entity.
        /// </summary>
        public decimal BerletiDij
        {
            get { return this.berletidij; }
            set { this.Set(ref this.berletidij, value); }
        }

        /// <summary>
        /// Gets or sets Negyzetmeter proprerty of Ingatlan entity.
        /// </summary>
        public decimal Negyzetmeter
        {
            get { return this.negyzetmeter; }
            set { this.Set(ref this.negyzetmeter, value); }
        }

        /// <summary>
        /// Convert VM entity to a DB entity.
        /// </summary>
        /// <returns>A DB entity.</returns>
        public Ingatlanok ConvertToDBEntity()
        {
            Ingatlanok vissza = new Ingatlanok();
            vissza.BerletiDij = this.BerletiDij;
            if (this.Butorozott)
            {
                vissza.Butorozott = 0;
            }
            else
            {
                vissza.Butorozott = 1;
            }

            vissza.IngatlanId = this.IngatlanId;
            vissza.Kerulet = this.Kerulet;
            vissza.Menedzser = this.Menedzser;
            vissza.Negyzetmeter = this.Negyzetmeter;
            switch (this.Tipus)
            {
                case EnumIngatlanType.Panel: vissza.Tipus = "Panel";
                    break;
                case EnumIngatlanType.Kertesház: vissza.Tipus = "Kertesház";
                    break;
                case EnumIngatlanType.Garázs: vissza.Tipus = "Garázs";
                    break;
            }

            return vissza;
        }

        /// <summary>
        /// Makes a copy of this entity.
        /// </summary>
        /// <param name="other">Object to copy.</param>
        public void CopyFrom(Ingatlan other)
        {
            PassThroughNonNull(other);
            this.Negyzetmeter = other.Negyzetmeter;
            this.Menedzser = other.Menedzser;
            this.Kerulet = other.Kerulet;
            this.BerletiDij = other.BerletiDij;
            this.Butorozott = other.Butorozott;
            this.Tipus = other.Tipus;
            this.IngatlanId = other.IngatlanId;
        }

        /// <summary>
        /// Check non-null.
        /// </summary>
        /// <param name="other">Ingatlan to check.</param>
        /// <returns>Ingatlan.</returns>
        private static Ingatlan PassThroughNonNull(Ingatlan other)
        {
            if (other == null)
            {
                throw new ArgumentNullException(nameof(other));
            }

            return other;
        }
    }
}
