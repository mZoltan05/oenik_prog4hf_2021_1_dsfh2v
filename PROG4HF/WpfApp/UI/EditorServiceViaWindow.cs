﻿// <copyright file="EditorServiceViaWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using WpfApp.BL;
    using WpfApp.Data;

    /// <summary>
    /// IEditorService implement.
    /// </summary>
    public class EditorServiceViaWindow : IEditorService
    {
        /// <inheritdoc/>
        public bool EditIngatlan(Ingatlan i)
        {
            EditorWindow win = new EditorWindow(i);
            return win.ShowDialog() ?? false;
        }
    }
}
