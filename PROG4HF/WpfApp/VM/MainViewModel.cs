﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using WpfApp.BL;
    using WpfApp.Data;

    /// <summary>
    /// View Model class of Main Window.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private Ingatlan kivalasztott;
        private IIngatlanLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IIngatlanLogic>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic">logic to use.</param>
        public MainViewModel(IIngatlanLogic logic)
        {
            this.logic = logic;
            this.Kiadok = new ObservableCollection<Ingatlan>();
            if (this.IsInDesignMode)
            {
                Ingatlan ingatlan1 = new Ingatlan();
                ingatlan1.Menedzser = "Új Menedzser";
                ingatlan1.Tipus = EnumIngatlanType.Garázs;
                ingatlan1.BerletiDij = 120000;
                ingatlan1.Butorozott = true;
                ingatlan1.IngatlanId = 120;
                ingatlan1.Kerulet = 5;
                ingatlan1.Negyzetmeter = 50;

                Ingatlan ingatlan2 = new Ingatlan();
                ingatlan2.Menedzser = "Másik Menedzser";
                ingatlan2.Tipus = EnumIngatlanType.Garázs;
                ingatlan2.BerletiDij = 80000;
                ingatlan2.Butorozott = false;
                ingatlan2.IngatlanId = 121;
                ingatlan2.Kerulet = 1;
                ingatlan2.Negyzetmeter = 55;

                this.Kiadok.Add(ingatlan1);
                this.Kiadok.Add(ingatlan2);
            }
            else
            {
                this.Kiadok = (ObservableCollection<Ingatlan>)logic.GetAllIngatlan();
            }

            this.AddCmd = new RelayCommand(() => this.logic.AddIngatlan(this.Kiadok));
            this.ModCmd = new RelayCommand(() => this.logic.UpdateIngatlan(this.Kivalasztott));
            this.DelCmd = new RelayCommand(() => this.logic.DeleteIngatlan(this.Kiadok, this.kivalasztott));
        }

        /// <summary>
        /// Gets collection of ingatlans.
        /// </summary>
        public ObservableCollection<Ingatlan> Kiadok { get; private set; }

        /// <summary>
        /// Gets or sets kivalasztott.
        /// </summary>
        public Ingatlan Kivalasztott
        {
            get { return this.kivalasztott; } set { this.Set(ref this.kivalasztott, value); }
        }

        /// <summary>
        /// Gets add.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets mod.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets del.
        /// </summary>
        public ICommand DelCmd { get; private set; }
    }
}
