﻿// <copyright file="IngatlanType.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Enum of ingatlan types.
    /// </summary>
    public enum EnumIngatlanType
    {
        /// <summary>
        /// Panel enum.
        /// </summary>
        Panel,

        /// <summary>
        /// Kertesház enum.
        /// </summary>
        Kertesház,

        /// <summary>
        /// Garázs enum.
        /// </summary>
        Garázs,
    }
}
