﻿// <copyright file="EditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;
    using WpfApp.Data;

    /// <summary>
    /// View Model class of Editor.
    /// </summary>
    public class EditorViewModel : ViewModelBase
    {
        private Ingatlan ingatlan;
        private string menedzser;
        private int berletidij;
        private bool butorozott;
        private EnumIngatlanType tipus;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            if (this.IsInDesignMode)
            {
                this.ingatlan = new Ingatlan();
                this.ingatlan.Menedzser = "Új Menedzser";
                this.ingatlan.Tipus = EnumIngatlanType.Garázs;
                this.ingatlan.IngatlanId = 999;
                this.ingatlan.Kerulet = 50;
                this.ingatlan.BerletiDij = 100000;
                this.ingatlan.Butorozott = true;
                this.ingatlan.Negyzetmeter = 1000;
            }
        }

        /// <summary>
        /// Gets to list all tipus.
        /// </summary>
        public static Array Tipusok
        {
            get { return Enum.GetValues(typeof(EnumIngatlanType)); }
        }

        /// <summary>
        /// Gets or sets ingatlan.
        /// </summary>
        public Ingatlan Ingatlan
        {
            get { return this.ingatlan; }
            set { this.Set(ref this.ingatlan, value); }
        }

        /// <summary>
        /// Gets or sets menedzser.
        /// </summary>
        public string Menedzser
        {
            get { return this.menedzser; }
            set { this.Set(ref this.menedzser, value); }
        }

        /// <summary>
        /// Gets or sets berletidij.
        /// </summary>
        public int Berletidij
        {
            get { return this.berletidij; }
            set { this.Set(ref this.berletidij, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether.
        /// </summary>
        public bool Butorozott
        {
            get { return this.butorozott; }
            set { this.Set(ref this.butorozott, value); }
        }

        /// <summary>
        /// Gets or sets berletidij.
        /// </summary>
        public EnumIngatlanType Tipus
        {
            get { return this.tipus; }
            set { this.Set(ref this.tipus, value); }
        }
    }
}
