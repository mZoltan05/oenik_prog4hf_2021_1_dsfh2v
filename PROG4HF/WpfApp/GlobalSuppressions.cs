﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: System.CLSCompliant(false)]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<NikGitStats>", Scope = "member", Target = "~M:WpfApp.VM.MainViewModel.#ctor(WpfApp.BL.IIngatlanLogic)")]