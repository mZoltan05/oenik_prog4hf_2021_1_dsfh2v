﻿// <copyright file="MyIoc.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;

    /// <summary>
    /// To fix a dotnet bug.
    /// </summary>
    public class MyIoc : SimpleIoc, IServiceLocator
    {
    /// <summary>
    /// Gets instance.
    /// </summary>
        public static MyIoc Instance { get; private set; } = new MyIoc();
    }
}
