﻿// <copyright file="IIngatlanLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using WpfApp.Data;

    /// <summary>
    /// Define the crud operations.
    /// </summary>
    public interface IIngatlanLogic
    {
        /// <summary>
        /// Create a new Ingatlan entity.
        /// </summary>
        /// <param name="list">List what contains the Ingatlans.</param>
        void AddIngatlan(IList<Ingatlan> list);

        /// <summary>
        /// Update an Ingatlan entity.
        /// </summary>
        /// <param name="ingatlanToUpdate">Ingatlan to Update.</param>
        void UpdateIngatlan(Ingatlan ingatlanToUpdate);

        /// <summary>
        /// Delete an Ingatlan entity.
        /// </summary>
        /// <param name="list">List what stores the Ingatlans.</param>
        /// <param name="ingatlan">Ingatlan to delete.</param>
        void DeleteIngatlan(IList<Ingatlan> list, Ingatlan ingatlan);

        /// <summary>
        /// Get the Ingatlans from DB.
        /// </summary>
        /// <returns>all of ingatlan.</returns>
        IList<Ingatlan> GetAllIngatlan();
    }
}
