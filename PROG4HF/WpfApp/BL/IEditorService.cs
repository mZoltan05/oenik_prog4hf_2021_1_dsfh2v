﻿// <copyright file="IEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using WpfApp.Data;

    /// <summary>
    /// Service to edit Ingatlan, DI.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// Declare that the Edit ends with OK or Cancel.
        /// </summary>
        /// <param name="i">Ingatlan to Edit.</param>
        /// <returns>OK or Cancel.</returns>
        bool EditIngatlan(Ingatlan i);
    }
}
