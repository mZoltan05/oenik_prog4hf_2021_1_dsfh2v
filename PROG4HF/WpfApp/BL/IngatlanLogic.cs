﻿// <copyright file="IngatlanLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;
    using Logicc;
    using WpfApp.Data;
    using WpfApp.UI;

    /// <summary>
    /// Implement the logic.
    /// </summary>
    public class IngatlanLogic : IIngatlanLogic
    {
        private IEditorService editorService;
        private IMessenger messengerService;
        private BusinessLogic logika = new BusinessLogic();

        /// <summary>
        /// Initializes a new instance of the <see cref="IngatlanLogic"/> class.
        /// </summary>
        /// <param name="editorService">Editor.</param>
        /// <param name="messengerService">Messenger.</param>
        public IngatlanLogic(IEditorService editorService, IMessenger messengerService)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
        }

        /// <inheritdoc/>
        public IList<Ingatlan> GetAllIngatlan()
        {
            IList<Ingatlan> ingatlanok = new ObservableCollection<Ingatlan>();
            Ingatlan uj;
            var lista = this.logika.ReadIngatlan();
            foreach (var item in lista)
            {
                uj = new Ingatlan();
                uj.BerletiDij = item.BerletiDij;
                if (item.Butorozott == 0)
                {
                    uj.Butorozott = false;
                }
                else
                {
                    uj.Butorozott = true;
                }

                uj.IngatlanId = item.IngatlanId;
                uj.Kerulet = item.Kerulet;
                uj.Menedzser = item.Menedzser;
                uj.Negyzetmeter = item.Negyzetmeter;
                switch (item.Tipus)
                {
                    case "Panel": uj.Tipus = VM.EnumIngatlanType.Panel; break;
                    case "Kertesház": uj.Tipus = VM.EnumIngatlanType.Kertesház; break;
                    case "Garázs": uj.Tipus = VM.EnumIngatlanType.Garázs; break;
                    default: uj.Tipus = VM.EnumIngatlanType.Kertesház; break;
                }

                ingatlanok.Add(uj);
            }

            return ingatlanok;
        }

        /// <inheritdoc/>
        public void AddIngatlan(IList<Ingatlan> list)
        {
            PassThroughNonNull(list);
            Ingatlan newIngatlan = new Ingatlan();
            if (this.editorService.EditIngatlan(newIngatlan) == true)
            {
                    list.Add(newIngatlan);
                    this.logika.CreateIngatlan(newIngatlan.ConvertToDBEntity());
                    this.messengerService.Send("Hozzáadva", "LogicResult");
            }
            else
            {
                this.messengerService.Send("Hozzáadás visszavonva", "LogicResult");
            }
        }

        /// <inheritdoc/>
        public void DeleteIngatlan(IList<Ingatlan> list, Ingatlan ingatlan)
        {
            PassThroughNonNull(list);
            CultureInfo provider = new CultureInfo("hu-HU");
            if (ingatlan != null && list.Remove(ingatlan))
            {
                this.logika.DeleteIngatlan(Convert.ToString(ingatlan.IngatlanId, provider));
                this.messengerService.Send("Törölve", "LogicResult");
            }
            else
            {
                this.messengerService.Send("Törlés nem sikerült", "LogicResult");
            }
        }

        /// <inheritdoc/>
        public void UpdateIngatlan(Ingatlan ingatlanToUpdate)
        {
            if (ingatlanToUpdate == null)
            {
                this.messengerService.Send("Módosítás HIBA", "LogicResult");
                return;
            }

            CultureInfo provider = new CultureInfo("hu-HU");
            string id = Convert.ToString(ingatlanToUpdate.IngatlanId, provider);
            Ingatlan clone = new Ingatlan();
            clone.CopyFrom(ingatlanToUpdate);
            if (this.editorService.EditIngatlan(clone) == true)
            {
                UpdateDB(id, ingatlanToUpdate, clone, this.logika, provider);
                ingatlanToUpdate.CopyFrom(clone);
                this.messengerService.Send("Módosítás sikeres", "LogicResult");
            }
            else
            {
                this.messengerService.Send("Módosítás visszavonva", "LogicResult");
            }
        }

        private static void UpdateDB(string id, Ingatlan oldIngatlan, Ingatlan newIngatlan, BusinessLogic logika, IFormatProvider provider)
        {
            if (oldIngatlan.BerletiDij != newIngatlan.BerletiDij)
            {
                logika.UpdateIngatlan(id, "BerletiDij", newIngatlan.BerletiDij.ToString(provider));
            }

            if (oldIngatlan.Butorozott != newIngatlan.Butorozott)
            {
                if (newIngatlan.Butorozott == true)
                {
                    logika.UpdateIngatlan(id, "Butorozott", "1");
                }
                else
                {
                    logika.UpdateIngatlan(id, "Butorozott", "0");
                }
            }

            if (oldIngatlan.Menedzser != newIngatlan.Menedzser)
            {
                logika.UpdateIngatlan(id, "Menedzser", newIngatlan.Menedzser);
            }

            // if (oldIngatlan.IngatlanId != newIngatlan.IngatlanId)
            // {
            //    logika.UpdateIngatlan(id, "IngatlanId", newIngatlan.IngatlanId.ToString(provider));
            // }
            if (oldIngatlan.Kerulet != newIngatlan.Kerulet)
            {
                logika.UpdateIngatlan(id, "Kerulet", newIngatlan.Kerulet.ToString(provider));
            }

            if (oldIngatlan.Negyzetmeter != newIngatlan.Negyzetmeter)
            {
                logika.UpdateIngatlan(id, "Negyzetmeter", newIngatlan.Negyzetmeter.ToString(provider));
            }

            if (oldIngatlan.Tipus != newIngatlan.Tipus)
            {
                logika.UpdateIngatlan(id, "Tipus", newIngatlan.Tipus.ToString());
            }
        }

        /// <summary>
        /// Check non-null.
        /// </summary>
        /// <param name="list">List to check.</param>
        /// <returns>list.</returns>
        private static IList<Ingatlan> PassThroughNonNull(IList<Ingatlan> list)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }

            return list;
        }
    }
}
