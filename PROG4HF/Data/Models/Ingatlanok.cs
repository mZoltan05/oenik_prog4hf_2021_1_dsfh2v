﻿// <copyright file="Ingatlanok.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.Models
{
    /// <summary>
    /// Ingatlan Entites.
    /// </summary>
    public partial class Ingatlanok
    {
        /// <summary>
        /// Gets or sets IngatlanId proprerty of Ingatlan entity.
        /// </summary>
        public decimal IngatlanId { get; set; }

        /// <summary>
        /// Gets or sets Tipus proprerty of Ingatlan entity.
        /// </summary>
        public string Tipus { get; set; }

        /// <summary>
        /// Gets or sets Menedzser proprerty of Ingatlan entity.
        /// </summary>
        public string Menedzser { get; set; }

        /// <summary>
        /// Gets or sets Kerulet proprerty of Ingatlan entity.
        /// </summary>
        public decimal Kerulet { get; set; }

        /// <summary>
        /// Gets or sets Butorozott proprerty of Ingatlan entity.
        /// </summary>
        public decimal Butorozott { get; set; }

        /// <summary>
        /// Gets or sets BerletiDij proprerty of Ingatlan entity.
        /// </summary>
        public decimal BerletiDij { get; set; }

        /// <summary>
        /// Gets or sets Negyzetmeter proprerty of Ingatlan entity.
        /// </summary>
        public decimal Negyzetmeter { get; set; }

        /// <summary>
        /// Override the ToString method.
        /// </summary>
        /// <returns>Ingatlan entity converted to string.</returns>
        public override string ToString()
        {
            return $"{this.IngatlanId} - {this.Tipus} - {this.Menedzser} - {this.Negyzetmeter}";
        }
    }
}
