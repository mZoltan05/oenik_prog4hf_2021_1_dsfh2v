﻿// <copyright file="IngatlanContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#nullable disable

namespace Data.Models
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata;

    /// <summary>
    /// Ingatlan Database.
    /// </summary>
    public partial class IngatlanContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IngatlanContext"/> class.
        /// Constructor of Ingatlan database.
        /// </summary>
        public IngatlanContext()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IngatlanContext"/> class.
        /// </summary>
        /// <param name="options"> Parameter called options.</param>
        public IngatlanContext(DbContextOptions<IngatlanContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Gets or sets set Ingatlanok.
        /// </summary>
        public virtual DbSet<Ingatlanok> Ingatlanoks { get; set; }

        /// <summary>
        /// Override OnConfiguring method.
        /// </summary>
        /// <param name="optionsBuilder">Parameter called optionsBuilder.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\Database.mdf;Integrated Security=True");
            }
        }

        /// <summary>
        /// Override OnModelCreating method.
        /// </summary>
        /// <param name="modelBuilder">Parameter called modelBuilder.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Ingatlanok>(entity =>
            {
                entity.HasKey(e => e.IngatlanId)
                    .HasName("PK__ingatlan__A83546384825AA3E");

                entity.ToTable("ingatlanok");

                entity.Property(e => e.IngatlanId)
                    .HasColumnType("numeric(4, 0)")
                    .HasColumnName("ingatlan_id");

                entity.Property(e => e.BerletiDij)
                    .HasColumnType("numeric(11, 0)")
                    .HasColumnName("berleti_dij");

                entity.Property(e => e.Butorozott)
                    .HasColumnType("numeric(1, 0)")
                    .HasColumnName("butorozott");

                entity.Property(e => e.Kerulet)
                    .HasColumnType("numeric(2, 0)")
                    .HasColumnName("kerulet");

                entity.Property(e => e.Menedzser)
                    .HasMaxLength(35)
                    .IsUnicode(false)
                    .HasColumnName("menedzser");

                entity.Property(e => e.Negyzetmeter)
                    .HasColumnType("numeric(4, 0)")
                    .HasColumnName("negyzetmeter");

                entity.Property(e => e.Tipus)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("tipus");
            });

            this.OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
